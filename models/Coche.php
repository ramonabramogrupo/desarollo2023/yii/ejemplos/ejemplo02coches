<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coche".
 *
 * @property string $bastidor
 * @property string|null $marca
 * @property string|null $modelo
 * @property int|null $cilindrada
 */
class Coche extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coche';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bastidor'], 'required'],
            [['cilindrada'], 'integer'],
            [['bastidor', 'marca', 'modelo'], 'string', 'max' => 100],
            [['bastidor'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bastidor' => 'Bastidor',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'cilindrada' => 'Cilindrada',
        ];
    }
}
