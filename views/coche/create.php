<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Coche $model */

$this->title = 'Nuevo Coche';
$this->params['breadcrumbs'][] = ['label' => 'Coches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coche-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
