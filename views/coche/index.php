<?php

use app\models\Coche;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Coches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coche-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            'Nuevo Coche', // texto del enlace
            ['/coche/create'], // /controlador/accion ==> accion
            ['class' => 'btn btn-primary'] // atributos de la etiqueta
            ) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider, // la consulta preparada con dataProvider
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'], // columna con numero de registro
            'bastidor',
            'marca',
            'modelo',
            'cilindrada',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Coche $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'bastidor' => $model->bastidor]);
                 }
            ],
        ],
    ]); ?>


</div>
