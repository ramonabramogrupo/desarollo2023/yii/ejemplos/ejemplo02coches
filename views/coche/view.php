<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Coche $model */

$this->title = $model->bastidor;
$this->params['breadcrumbs'][] = ['label' => 'Coches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="coche-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
            'Actualizar', // texto
            ['update',  // accion del controlador coche
            'bastidor' => $model->bastidor // datos a mandar a la accion
            ], 
            ['class' => 'btn btn-primary'] // aspecto del boton
            ) ?>
        <?= Html::a('Eliminar', ['delete', 'bastidor' => $model->bastidor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de que quieres eliminar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model, // el registro a mostrar
        'attributes' => [ // campos a mostrar
            'bastidor',
            'marca',
            'modelo',
            'cilindrada',
        ],
    ]) ?>

        <div>Bastidor: <?= $model->bastidor ?></div>
</div>
