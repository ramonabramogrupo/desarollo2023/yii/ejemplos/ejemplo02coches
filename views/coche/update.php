<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Coche $model */

$this->title = 'Actualizando ' . $model->bastidor;
$this->params['breadcrumbs'][] = ['label' => 'Coches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bastidor, 'url' => ['view', 'bastidor' => $model->bastidor]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="coche-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
